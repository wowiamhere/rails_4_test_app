ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

# for advanced testing
require 'minitest/reporters'
Minitest::Reporters.use!
# for advanced testing

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

#to use title helper in tests
	include ApplicationHelper
	include TrendzSessionsHelper
#to use title helper in tests

#to check log in status in test
	def is_logged_in_tst?
		!session[:fashion_trendz_user_id].nil?
	end
#to check log in status in test


#test user login
	def log_in_as_tst(usr, opts = {})
		pass = opts[:password] || 'password'
		rem_me = opts[:remember_me] || '1'
		
		if integration_test?
			post login_path, trendz_session: { email: usr.email, password: pass, remember_me: rem_me }
		else
			session[:fashion_trendz_user_id] = usr.id
		end			
	end

	private
		def integration_test?
			defined?(post_via_redirect)
		end
end
