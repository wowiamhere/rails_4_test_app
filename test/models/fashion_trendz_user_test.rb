require 'test_helper'

class FashionTrendzUserTest < ActiveSupport::TestCase
	def setup
		@fashion_user = FashionTrendzUser.new(name: "fnTest lnTest", email: "fnTest@trendzblogtest.com", password: "xxxxxxx", password_confirmation: "xxxxxxx")
	end

	test 'fashion blog user must be valid' do 
		assert @fashion_user.valid?
	end

	test 'fashion blog user name must be present' do
		@fashion_user.name = '     '
		assert_not @fashion_user.valid?
	end

	test 'fashion blog user email must be present' do
		@fashion_user.email = '   '
		assert_not @fashion_user.valid?
	end

	test 'fashion blog user name too long' do 
		@fashion_user.name = 'a' * 51
		assert_not @fashion_user.valid?
	end

	test 'fashion blog user email too long' do
		@fashion_user.email = 'a' * 244 + '@trendzblogtest.com'
		assert_not @fashion_user.valid?
	end

	test 'fashion blog user email valid' do 
		ok_email = %w[trendzUser@trendzblogtest.com USER@lala.COM A_uS-Er@this.that.org	first.last@coo.js tina+gina@rina.in]
		ok_email.each do |kemail|
			@fashion_user.email = kemail
			assert @fashion_user.valid?, "#{kemail.inspect} should be a valid email"
		end
	end

	test 'fashion blog user email not valid!!!' do 
		not_ok_email = %w[user@example,com user_at_foo.org user.name@example.this@that_this.com that@this+another.com]
		not_ok_email.each do |nkemail|
			@fashion_user.email = nkemail
			assert_not @fashion_user.valid?, '#{nkemail.inspect} is not valid'
		end
	end

	test 'fashion blog user email uniquenes' do 
		dup_fashion_user = @fashion_user.dup
		dup_fashion_user.email = @fashion_user.email.upcase
		@fashion_user.save
		assert_not dup_fashion_user.valid?
	end

	test 'fashion trendz email should be saved in lower-case' do 
		mix_case_trendz_email = 'aBcDE@FghiKJ.cOM'
		@fashion_user.email = mix_case_trendz_email
		@fashion_user.save
		assert_equal mix_case_trendz_email.downcase, @fashion_user.reload.email
	end
	
	test 'fashion trendz user password should be non-blank' do
		@fashion_user.password = @fashion_user.password_confirmation = " " * 6
		assert_not @fashion_user.valid?
	end

	test 'fashion trendz user password does not meet minimum lenght' do
		@fashion_user.password = @fashion_user.password_confirmation = "z" * 5
		assert_not @fashion_user.valid?
	end

	test 'authenticated? must return false with nil digest' do 
		assert_not @fashion_user.authenticated?('')
	end

end
