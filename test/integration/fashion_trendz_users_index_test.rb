require 'test_helper'

class FashionTrendzUsersIndexTest < ActionDispatch::IntegrationTest
	def setup
		@admin = fashion_trendz_users(:testUser)
		@non_admin = fashion_trendz_users(:testUser2)
	end

	test 'index as admin including pagination and delete links' do 
		log_in_as_tst(@admin)
		get fashion_trendz_users_path
		assert_template 'fashion_trendz_users/index'
		assert_select 'div.pagination'

		first_page_of_users = FashionTrendzUser.paginate(page: 1)
		first_page_of_users.each do |usr| 
			assert_select 'a[href=?]', fashion_trendz_user_path(usr), text: user.name
			unless usr == @admin 
				assert_select 'a[href=?]', fashion_trendz_user_path(usr), text: 'delete'
			end
		end
		assert_difference "FashionTrendzUser.count", -1 do 
			delete user_path(@non_admin)
		end
	end

	test 'index as non-admin' do 
		log_in_as_tst(@non_admin)
		get fashion_trendz_users_path
		assert_select 'a', text: 'delete', count: 0
	end

end
