require 'test_helper'

class LatestFashionTrendzBlogSiteLayoutTest < ActionDispatch::IntegrationTest
	test "latest_fashion_trendz_blog_layout_links" do 
		get root_path
		assert_template 'latest_fashion_trendz/advice'
		assert_select 'a[href=?]', root_path, 		count: 2
		assert_select 'a[href=?]', guide_path,	 	count: 2
		assert_select 'a[href=?]', bios_path, 		count: 2
		assert_select 'a[href=?]', about_path,	 	count: 2
		assert_select 'a[href=?]', contact_path,	count: 2
		get signup_path
		assert_select "title", latest_fashion_trendz_blog_title("Sign Up!")
	end
end
