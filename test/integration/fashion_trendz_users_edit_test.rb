require 'test_helper'

class FashionTrendzUsersEditTest < ActionDispatch::IntegrationTest
	def setup
			@fashion_trendz_user = fashion_trendz_users(:testUser)
	end
=begin
	test 'unsuccessful edit' do 
		log_in_as_tst(@fashion_trendz_user)
		get edit_fashion_trendz_user_path(@fashion_trendz_user)
		assert_template 'fashion_trendz_users/edit'
		patch fashion_trendz_user_path(@fashion_trendz_user), fashion_trendz_user: { name: "", email: "invalid.com", password: "x", password_confirmation: "xx" }
		assert_template 'fashion_trendz_users/edit'
	end

	test 'succesful edit' do
		log_in_as_tst @fashion_trendz_user
		get edit_fashion_trendz_user_path @fashion_trendz_user 
		assert_template 'fashion_trendz_users/edit'
		nm = "new name"
		em = 'new@email.com'
		patch fashion_trendz_user_path(@fashion_trendz_user), fashion_trendz_user: { name: nm, email: em, password: "", password_confirmation: "" }
		assert_not flash.empty?
		assert_redirected_to @fashion_trendz_user
		@fashion_trendz_user.reload
		assert_equal nm, @fashion_trendz_user.name
		assert_equal em, @fashion_trendz_user.email
	end


	test 'succesful edit with friendly forwarding' do 
		get edit_fashion_trendz_user_path(@fashion_trendz_user)
		log_in_as_tst @fashion_trendz_user
		assert_redirected_to edit_fashion_trendz_user_path(@fashion_trendz_user)
		nm = "another name"
		em = 'another email'
		patch edit_fashion_trendz_user_path(@fashion_trendz_user), fashion_trendz_user: { name: nm, email: em, password: "", password_confirmation: "" }
		assert_not flash_empty?
		assert_redirected_to @fashion_trendz_user
		@fashion_trendz_user.reload
		assert_equal nm, @fashion_trendz_user.name
		assert_equal em, @fashion_trendz_user.email
	end
=end	
end
