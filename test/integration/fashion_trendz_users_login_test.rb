require 'test_helper'

class FashionTrendzUsersLoginTest < ActionDispatch::IntegrationTest

  def setup
    @fashion_trendz_user = fashion_trendz_users(:testUser)
  end

	test 'login with invalid information' do 
		get login_path
		assert_template 'trendz_sessions/new'
		post login_path, trendz_session: { email: "", password: "" }
		assert_template 'trendz_sessions/new'
		assert_not flash.empty?
		get root_path
		assert flash.empty?
	end
=begin
  test "login with valid information followed by logout" do
    get login_path
    post login_path, trendz_session: { email: @fashion_trendz_user.email, password: 'password', remember_me: '0' }
    assert is_logged_in_tst?
    assert_redirected_to @fashion_trendz_user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path, count: 2
    assert_select "a[href=?]", fashion_trendz_user_path(@fashion_trendz_user), count: 2
    delete logout_path
    assert_not is_logged_in_tst?
    assert_redirected_to root_url
#simulating logout in different window
    delete logout_path    
    follow_redirect!
    assert_select 'a[href=?]', login_path, count: 2
    assert_select 'a[href=?]', logout_path, count: 0
    assert_select 'a[href=?]', fashion_trendz_user_path(@fashion_trendz_user), count: 0
  end

  test 'login with remembering' do
    log_in_as_tst(@fashion_trendz_user, remember_me: '1')
    assert_equal cookies['remember_token'], assigns(:fashion_trendz_user_session).remember_token 
  end
=end
  test 'login without remembering' do 
    log_in_as_tst(@fashion_trendz_user, remember_me: '0')
    assert_nil cookies['remember_token']
  end

end
