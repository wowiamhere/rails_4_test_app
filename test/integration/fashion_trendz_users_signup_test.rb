require 'test_helper'

class FashionTrendzUsersSignupTest < ActionDispatch::IntegrationTest

	test 'invalid signup info' do 
		get signup_path
		assert_no_difference 'FashionTrendzUser.count' do 
			post fashion_trendz_users_path, fashion_trendz_user: { name: '', email: 'user@invalid', password: 'some', password_confirmation: 'other' }
		end
		assert_template 'fashion_trendz_users/new'
		assert_select 'div#error_explanation'
		assert_select 'div.alert-danger2'
	end

	test 'valid signup' do 
		get signup_path
		assert_difference 'FashionTrendzUser.count', 1 do 
			post_via_redirect fashion_trendz_users_path, fashion_trendz_user: { name: 'Trendz Example', email: 'trendzuser@trendzuser.com', password: 'password', password_confirmation: 'password' }
		end
		assert_template 'fashion_trendz_users/show'
		assert is_logged_in_tst?

	end

end
