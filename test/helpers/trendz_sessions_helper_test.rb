require 'test_helper'

class SessionsHelperTest < ActionView::TestCase

	def setup 
		@fashion_trendz_user = fashion_trendz_users(:testUser)
		remember @fashion_trendz_user
	end

	test 'curr_trendz_user returns correct user when session[:...] is nil' do 
		assert_equal @fashion_trendz_user, current_fashion_trendz_user
		assert is_logged_in_tst?
	end

	test 'curr_trendz_user returns nil when rem dig wrong' do 
		@fashion_trendz_user.update_attribute(:remember_digest, FashionTrendzUser.digest(FashionTrendzUser.new_token))
		assert_nil current_fashion_trendz_user		
	end

end