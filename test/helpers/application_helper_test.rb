require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
	test 'latest fashion trendz blog title helper' do 
		assert_equal latest_fashion_trendz_blog_title, 					'Latest Fashion Trendz Blog'
		assert_equal latest_fashion_trendz_blog_title('Test'),  'Latest Fashion Trendz Blog Test'	
	end
end