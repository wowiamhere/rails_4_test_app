require 'test_helper'

class LatestFashionTrendzControllerTest < ActionController::TestCase
  test "should get advice" do
    get :advice
    assert_response :success
    assert_select "title", latest_fashion_trendz_blog_title("Fashion Advice")
  end

  test "should get guide" do
    get :guide
    assert_response :success
    assert_select "title", latest_fashion_trendz_blog_title("Wardrove Guide")   
  end

  test "should get bios" do
    get :bios
    assert_response :success
    assert_select "title", latest_fashion_trendz_blog_title("Fashion World Bios")
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", latest_fashion_trendz_blog_title("About")    
  end

  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", latest_fashion_trendz_blog_title("Contact")
  end

end
