require 'test_helper'

class FashionTrendzUsersControllerTest < ActionController::TestCase
	def setup 
		@fashion_trendz_user = fashion_trendz_users(:testUser)
		@other_user = fashion_trendz_users(:testUser2)
	end

  test "should get new" do
    get :new
    assert_response :success
    assert_select "title", latest_fashion_trendz_blog_title("Sign Up!")
  end

  test 'should redirect EDIT when not logged in' do 
  	get :edit, id: @fashion_trendz_user
  	assert_not flash.empty?
  	assert_redirected_to login_url
  end
  test 'should redirect UPDATE when not logged in' do 
  	patch :update, id: @fashion_trendz_user, fashion_trendz_user: { name: @fashion_trendz_user.name, email: @fashion_trendz_user.email }
  	assert_not flash.empty?
  	assert_redirected_to login_url
  end

	test 'should redirect EDIT when logged as wrong user' do 
		log_in_as_tst(@other_user)
		get :edit, id: @fashion_trendz_user
		assert flash.empty?
		assert_redirected_to root_url
	end

	test 'should redirect UPDATE when logged as wrong user' do 
		log_in_as_tst(@other_user)
		patch :update, id:@fashion_trendz_user, fashion_trendz_user: { name: @fashion_trendz_user.name, email: @fashion_trendz_user.email }
		assert flash.empty?
		assert_redirected_to root_url
	end	

	test 'should redirect index when not logged in' do 
		get :index
		assert_redirected_to login_url
	end

	test 'should redirect destroy whne not logged in' do 
		assert_no_difference 'FashionTrendzUser.count' do 
			delete :destroy, id: @fashion_trendz_user
		end
		assert_redirected_to login_url
	end	

	test 'should redirect destroy whe logged in as a non-admin' do 
		log_in_as_tst(@other_user)
		assert_no_difference "FashionTrendzUser.count" do 
			delete :destroy, id: @fashion_trendz_user
		end
		assert_redirected_to root_url
	end
end
