# Preview all emails at http://localhost:3000/rails/mailers/fashion_trendz_user_mailer
class FashionTrendzUserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/fashion_trendz_user_mailer/account_activation
  def account_activation
  	usr = FashionTrendzUser.first
  	usr.activation_token = FashionTrendzUser.new_token
  	FashionTrendzUserMailer.account_activation(usr)
  end

  # Preview this email at http://localhost:3000/rails/mailers/fashion_trendz_user_mailer/password_reset
  def password_reset
    usr = FashionTrendzUser.first
    usr.reset_tok = FashionTrendzUser.new_token
    FashionTrendzUserMailer.password_reset(usr)
  end

end
