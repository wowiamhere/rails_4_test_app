# Ruby On Rails test Project
		
**CAPABILITIES**  

- Ruby on Rails 4.2.2
	+ 3 models
		* user, post, comment
		* associations
			+ has_many, belongs_to
		* currently expanding
		* validations
	+ controllers
		* posts, comments
		* session control
			+ sign-in/out
			+ remember me option
			+ friendly forwarding
			+ more to come
	+ pagination (backend: rails via will_paginate)  
- AngularJs 1.5
	* $route/ngView
		- 4 AngularJs templates
		- 3 AngularJs controllers
		- 2 AngularJs services
	* Directives throughout to:
		- render/delete comments within ngView templates
		- edit/create comments within ngView templates
	* $http
		- calls to backend without $resource
	* function to transform response (XML option)
		received from $http or methods in $resource
	* watching/changing $location.path() to navigate
		through information on same page
		+ returning url to original from backend
			when done with angularjs
	* in profile page
		- search comments by date
	* in main page sidebar
		- display comments by id#
	* if logged in
		- post comment via AngularJs (option)
			right next to comment text area from backend
- PostGreSql
- Bootstrap
- Bower
- Sass
- fully responsive (250x490/1200x1450)
	
- PostGreSql
	* 3 tables
		- users, posts, comments
	* associations
		* created respective id columns


**Live Website**  
https://rails4app.herokuapp.com/  

**Repo**  
https://bitbucket.org/wowiamhere/rails_4_test_app    

**Online Portfolio**  
http://ZenCodeMaster.com  


* Pre-existing accounts for testing:  
	Admin sign-in: examples@trendzblog.com  
			pass: password  
	User  sign-in: examples-n@trendzblog.com  
			pass: password  
		- n for users.  
		- seeds range from 1 to 100   
		- ex.  
			examples-1@trendzblog.com  
	+ Only Admins can create posts.  
	+ Admins & users with accounts can create comments on posts.  
* or CREATE A USER and browse around.  
	(NO weird comments OR posts least the wrath of the  
		Computing Gods Of The Universe shall befall upon thee.)  