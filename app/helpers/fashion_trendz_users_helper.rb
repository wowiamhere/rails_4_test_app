module FashionTrendzUsersHelper
# -to setup/retrieve profile image from gravatar.com
# -currently has a placeholder image from lorempixel.com
	def profile_img(user, options = { size: 80 })
		grav_hash = Digest::MD5::hexdigest(user.email.downcase)
		size = options[:size]
		grav_url = "https://secure.gravatar.com/avatar/#{grav_hash}?s=#{size}"
		image_tag "http://lorempixel.com/50/50/people", alt: "#{user.name}'s img", class: "cProfImg"
	end

end
