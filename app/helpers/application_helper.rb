module ApplicationHelper
# helper for pages title
	def latest_fashion_trendz_blog_title(page_title = "")
		common_title = "Latest Fashion Trendz Blog"
		if page_title.empty?
			common_title
		else
			common_title + ' ' + page_title
		end
	end

end
