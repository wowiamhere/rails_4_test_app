module TrendzSessionsHelper

	def log_in_trendz_user(user) #log_in(user)
		session[:fashion_trendz_user_id] = user.id
	end

#returns current logged-in user (if any)
	def current_fashion_trendz_user #current_user
		if (user_id = session[:fashion_trendz_user_id])
			@curr_fashion_trendz_user ||= FashionTrendzUser.find_by(id: user_id)
		elsif (user_id = cookies.signed[:fashion_trendz_user_id])
#raise
			user = FashionTrendzUser.find_by(id: user_id)
			if user && user.authenticated?(:remember, cookies[:remember_token])
				log_in_trendz_user user
				@curr_fashion_trendz_user = user
			end
		end
	end	

	#true if user logged in
	def logged_in?
		!current_fashion_trendz_user.nil?
	end

	def remember(usr)
		usr.remember
		cookies.permanent.signed[:fashion_trendz_user_id] = usr.id
		cookies.permanent[:remember_token] = usr.remember_token
	end

	def curr_user?(usr)
		usr == current_fashion_trendz_user
	end

	def curr_user_admin?
		current_fashion_trendz_user.admin
	end

	def forget(usr)
		usr.forget
		cookies.delete(:fashion_trendz_user_id)
		cookies.delete(:remember_token)
	end	

	#log out
	def log_out_trendz_user
		forget(current_fashion_trendz_user)
		session.delete(:fashion_trendz_user_id)
		@curr_fashion_trendz_user = nil
	end

#friendly fw
	#redirect to stored location
	def red_back_or(df)
		redirect_to(session[:fw_url] || df)
		session.delete(:fw_url)
	end
	#stores url
	def store_loc(req = request.url)
		session[:fw_url] = req if request.get?
	end

end
