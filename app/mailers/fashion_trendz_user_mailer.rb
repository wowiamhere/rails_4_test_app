class FashionTrendzUserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.fashion_trendz_user_mailer.account_activation.subject
  #
  def account_activation(usr)
    @fashion_trendz_user = usr

    mail to: usr.email, subject: "Account Activation"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.fashion_trendz_user_mailer.password_reset.subject
  #
  def password_reset(usr)
    @user = usr
    mail to: usr.email, subject: "Password Reset (instructions)"
  end
end
