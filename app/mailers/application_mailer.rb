class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@trendzblog.com"
  layout 'mailer'
end
