# Posts 
# 	-only admins can post Posts
# 	-any one can comment on a post once logged in

class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy

	default_scope -> { order(created_at: :desc) }

	validates :content, presence: true, length: { maximum: 2500 }

end
