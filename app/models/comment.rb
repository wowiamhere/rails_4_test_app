# Comment model for application
# 	-logged in user able to post several comments
# 		via front/back-end throughout the application pages
# 	-user can view/edit/delete comments from profile page
# 	-user can edit/create comments from front (home) page
# 	-various comments seeded into database

class Comment < ActiveRecord::Base

  belongs_to :fashion_trendz_user
  belongs_to :post

  default_scope -> { order(created_at: :desc) }

  validates :fashion_trendz_user_id, presence: true
  validates :content, 							 presence: true, length: { maximum: 750 }
  validates :post_id,								 presence: true
  
end
