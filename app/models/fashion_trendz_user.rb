# Application users model
#   -currently only 1 administrator
#   -varius users seeded into database

class FashionTrendzUser < ActiveRecord::Base
  has_many :comments, dependent: :destroy

  attr_accessor :remember_token, :activation_token, :reset_tok

	before_save :downcase_email
  before_create :create_activation_digest

	EMAIL_REG_VALIDATION = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

	validates :name, 	presence: true, length: { maximum: 50 }
	validates :email, presence: true,	length: { maximum: 255 }, format: { with: EMAIL_REG_VALIDATION }, uniqueness: { case_sensitive: false }

	has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  def self.digest(str)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(str, cost: cost)
  end

  def self.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = FashionTrendzUser.new_token
    update_attribute(:remember_digest, FashionTrendzUser.digest(remember_token))
  end

# returns true if gien token matches digest  
  def authenticated?(attr, tok)
    dig = send("#{attr}_digest")
    return false if dig.nil?
    BCrypt::Password.new(dig).is_password?(tok)
  end

  def forget
    update_attribute(:remember_digest, nil)
  end

# account activation
  def activ
    update_columns(activated: true, activated_at: Time.zone.now)    
  end

# send email for acc activation
  def activ_email
    FashionTrendzUserMailer.account_activation(self).deliver_now
  end

# password reset
  def create_pass_res_dig
    self.reset_tok = FashionTrendzUser.new_token
    update_columns(reset_digest: FashionTrendzUser.digest(reset_tok), reset_sent_at: Time.zone.now)
  end

# def send reset password email
  def send_pass_res_em
    FashionTrendzUserMailer.password_reset(self).deliver_now
  end

# returns true if pass reset expired
  def pass_res_exp?
    reset_sent_at < 2.hours.ago
  end

  private

# downcase email
    def downcase_email
      self.email = email.downcase
    end 
# for activation token and digest
    def create_activation_digest
      self.activation_token = FashionTrendzUser.new_token
      self.activation_digest = FashionTrendzUser.digest(activation_token)
    end

end