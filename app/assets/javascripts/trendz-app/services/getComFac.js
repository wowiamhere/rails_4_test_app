trendz_app.factory("getComFac",function($resource){

	var targetUrl = "/api/comments/:id";
	var params = { id: "@id" };
	var methods = 	{
		'create': {	method: 'POST' },
		'index': { 
				method: 'GET',
				isArray: true,
				cache: false,
			},
		'show': 	{ 
				method: 'GET',
				isArray: false,
				cache: false,
/*				transformResponse: 
					function(data, headersGetter){			
						return TransformFunction(data);
				}*/
		},
		'update': 	{ method: 'PUT' },
		'destroy': 	{ method: 'DELETE' }
	};

/* option for xml responses */		
	function TransformFunction(dt, b){
				var xmlDoc = $.parseXML(dt), 
						$xml = $(xmlDoc);

				var dt = {};
				dt.content = $xml.find('content').text();
				dt.creat = $xml.find('created-at').text();
				return dt;		
	};

	return $resource(targetUrl, params, methods);
});