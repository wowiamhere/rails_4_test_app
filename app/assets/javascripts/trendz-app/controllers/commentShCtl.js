/* 		
for displaying any comments on side bar of
	-home page.
	-Able to display by comment id.
	-ready for further development
*/	
trendz_app.controller('commentShCtl', ['$scope', '$routeParams', '$http', 'getComFac',
	function($scope, $routeParams, $http, getComFac){

		console.log("commentsShCtl loaded!");
			
		$scope.comId = 1;
	
		$scope.$watch('comId',function(){
			getComFac.show({ id: $scope.comId }, function(data){
				$scope.comment = data.content;
				$scope.creat = data.created_at;
			});

		});
	}]);