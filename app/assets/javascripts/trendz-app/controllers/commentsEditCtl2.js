/* for editing comments via AngularJs.
		-used  with edit.html.erb (angular template)
		-takes existing comment
		-pops it up in form for editing
		-saves back to backend database
		-displays success/error to user
		-resets $location for use with $route/ngView
*/		

trendz_app.controller("commentsEditCtl2", ["$scope", "$location", "$routeParams", "$timeout", "$interval", "getComFac",
	function($scope, $location, $routeParams, $timeout, $interval, getComFac){
		console.log("commentsEditCtl2 loaded! ");

//array for errors
		$scope.postComErr = [];

//to display cancel button on angularjs 
//	partial on profile page
		$scope.canbool = true;

//then show comment within form
			$scope.comment = getComFac.show({ id: $routeParams.id });

//if hit submit button do this
		$scope.submit = function(){
			console.log("submit pressed. ");

//updatind time strap in object
		$scope.comment.updated_at = new Date();

//function to handle successful response
			function succ(resp){
				console.log("successfully received response. ", resp);
				$scope.succ = "New comment Posted via AngularJs.\n(needs reload to display comment)";
				$scope.comment.content = "";

// setting $location to user page to render initial Angular
//		$resource template renderes				
				$scope.newLoc = function(){
					$location.path('/fashion_trendz_users/' + $scope.comment.fashion_trendz_user_id);
				};

// allowing for user to see success response 
//	before redirecting via $location.path
				$timeout($scope.newLoc, 4000);

// for counter to angular redirect
				$scope.sec = 4;
				$scope.angCount = function(){
					$scope.sec = $scope.sec - 1;
				};
				$interval($scope.angCount, 1000);

			}

			//function to handle error
			function err(errResp){
				console.log("error in response. ", errResp['data']);

				angular.forEach(errResp['data'], function(val, key){
					this.push(key + "->" + val);
				}, $scope.postComErr );			
			}

			//if comment blank
			if(!$scope.comment['post_id']){
				$scope.postComErr.push("comment Cant' be blank!");
			}
			else{
				//if not, update comment
				getComFac.update($scope.comment, succ, err);
			}
		};

		// cancel button: AngularJs Routing to 
		//	templateUrl: index, controller: commentIdxCtl
		$scope.cancel = function(){
			console.log('hitting cancel');

			$location.path("/fashion_trendz_users/" + $scope.comment.fashion_trendz_user_id);
		};

		//for css class
		$scope.errCssClass = function(name){
			//getting form
			var s;
			s = $scope.form[name];
			//returning error string if $invalid & $dirty
			return (s.$invalid) && (s.$dirty) ? "error" : "NO ERROR";
		};

		//errors
		$scope.errorMessage = function(name){
			//getting forms errors
			var s;
			s = $scope.form[name].$error;
			result = [];
			$.each(s, function(key, value){
				result.push(key);
			});
			return result.join(", ");
		};
}]);