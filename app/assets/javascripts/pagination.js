/* for pagination of posts in home page */
$(document).ready(function(){
	if($('.pagination').length){
		$(window).on('scroll', function(){
			m_posts = $('.pagination .next_page a').attr('href');

			if(m_posts && $(window).scrollTop() > $(document).height() - $(window).height() - 50){
				$('.pagination').html('<span class="cH3Com" style="font-family:j;color:green">please wait...<span>');
				return $.getScript(m_posts);
			} 

		});
		return $(window).scroll();
	}
});