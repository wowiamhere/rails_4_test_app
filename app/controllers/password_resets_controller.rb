class PasswordResetsController < ApplicationController
	before_action :get_usr,						only: [:edit, :update]
	before_action :val_usr,						only: [:edit, :update]
	before_action :exp_pass_res_tok, 	only: [:edit, :update]

  def new
  end

  def create
  	@fashion_trendz_user = FashionTrendzUser.find_by(email: params[:password_reset][:email].downcase)
		if @fashion_trendz_user
			@fashion_trendz_user.create_pass_res_dig
			@fashion_trendz_user.send_pass_res_em
			flash[:info] = "Email sent with password reset instructions"
			redirect_to root_url
		else
			flash.now[:danger] = "Email Addres NOT found!"
			render 'new'
		end
  end

  def edit
  end

  def update
  	if params[:fashion_trendz_user][:password].empty?
  		@fashion_trendz_user.errors.add(:password, "can't be empty")
  		render 'edit'
  	elsif @fashion_trendz_user.update_attributes(user_params)
  		log_in_trendz_user @fashion_trendz_user
  		flash[:success] = "Password has been reset.!"
  		redirect_to @fashion_trendz_user
  	else
  		render 'edit'
  	end
  end

  private

  	def user_params
  		params.require(:fashion_trendz_user).permit(:password, :password_confirmation)
  	end

		def get_usr
			@fashion_trendz_user = FashionTrendzUser.find_by(email: params[:email])
		end
# valid user?
		def val_usr
			unless (@fashion_trendz_user && @fashion_trendz_user.activated? && @fashion_trendz_user.authenticated?(:reset, params[:id]))
				redirect_to root_url
			end
		end
# expiration of reset tokn
		def exp_pass_res_tok
			if @fashion_trendz_user.pass_res_exp?
				flash[:danger] = "Password reset has expired. Enter Email for new isntructions."
				redirect_to new_password_reset_url
			end
		end
end
