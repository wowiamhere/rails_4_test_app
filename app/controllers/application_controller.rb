class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include TrendzSessionsHelper

  private

    def auth 
      # -for authorization (is logged in or redirect to login)
      # -used in
      #   *posts_controller      
      #   *fashion_trendz_users_controller
      unless logged_in?
        store_loc
        flash[:danger] = 'Please Log In!.'
        redirect_to login_url
      end
    end 

    
end
