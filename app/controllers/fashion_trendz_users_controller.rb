class FashionTrendzUsersController < ApplicationController
  before_action :auth,      only: [:index, :show, :edit, :update]
  before_action :corr_user, only: [:show, :edit, :update]
  before_action :is_admin?, only: [:destroy]

  def index
    @fashion_trendz_users = FashionTrendzUser.where(activated: true).paginate(page: params[:page])
  end

  def show
  	@fashion_trendz_user = FashionTrendzUser.find(params[:id])
    redirect_to root_url and return unless @fashion_trendz_user.activated?

# instantiation post to create and save through
#   fashion_trendz_user views
    unless !current_fashion_trendz_user.admin?
      @post = Post.new
    end

  end
    
  def new
  	@fashion_trendz_user = FashionTrendzUser.new
  end

  def create
  	@fashion_trendz_user = FashionTrendzUser.new(fashion_trendz_user_params)
  	if @fashion_trendz_user.save
      @fashion_trendz_user.activ_email
      flash[:success] = 'Please check your email to Activate your Fashion Trendz Account!'
  		redirect_to root_url
  	else
  		render 'new'
  	end
  end	

  def edit 
    @fashion_trendz_user = FashionTrendzUser.find(params[:id])
  end

  def update
    @fashion_trendz_user = FashionTrendzUser.find(params[:id])
    if @fashion_trendz_user.update_attributes(fashion_trendz_user_params)
      flash[:success] = "Profile Updated!"
      redirect_to @fashion_trendz_user      
    else
      render 'edit'
    end
  end

  def destroy
    FashionTrendzUser.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to fashion_trendz_users_url
  end

  private

    def fashion_trendz_user_params
    	params.require(:fashion_trendz_user).permit(:name, :email, :password, :password_confirmation)
    end

#before filters

  #for coroboratin correct user trying to edit/update
    def corr_user 
      @fashion_trendz_user = FashionTrendzUser.find(params[:id])
      redirect_to(root_url) unless (curr_user?(@fashion_trendz_user) || curr_user_admin?)
    end

    def is_admin?
      redirect_to(root_url) unless current_fashion_trendz_user.admin?
    end

end