class CommentsController < ApplicationController
	def index
	end
	def new
	end
	def show
	end
	def create
		@created_comment = Comment.create!(new_com_params)
		redirect_to root_url
	end
	def edit
	end
	def update
		@upCom = Comment.find(params[:id])
		if(@upCom.update_attributes(new_com_params))
			redirect_to root_url
		end
	end
	def destroy
	end

	private

# for creating new comment
# original var passed to form in _comment_form.html.erb
# created in post controller via private method
		def new_com_params
			params.require(:comment).permit(:content, :fashion_trendz_user_id, :post_id)
		end
end
