class AccountActivationsController < ApplicationController
# finding user to authenticate and activate accout
# 	once email link comes back		
	def edit		
		usr = FashionTrendzUser.find_by(email: params[:email])
		if usr && !usr.activated? && usr.authenticated?(:activation, params[:id])
			usr.activ
			log_in_trendz_user usr
			flash[:success] = "Account Activated!"
			redirect_to usr
		else
			flash[:danger] = "Invalid Activation Link"
			redirect_to root_url
		end
	end
end
