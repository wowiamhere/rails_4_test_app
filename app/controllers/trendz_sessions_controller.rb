class TrendzSessionsController < ApplicationController
  def new
  end

  def create
  	@fashion_trendz_user = FashionTrendzUser.find_by(email: params[:trendz_session][:email].downcase)
		if @fashion_trendz_user && @fashion_trendz_user.authenticate(params[:trendz_session][:password])
        if @fashion_trendz_user.activated?
          log_in_trendz_user @fashion_trendz_user
          params[:trendz_session][:remember_me] == '1' ? remember(@fashion_trendz_user) : forget(@fashion_trendz_user)
          red_back_or @fashion_trendz_user
        else
          msg = "Account not activated. "
          msg += "Check your email for the activation link."
          flash[:warning] = message
          redirect_to root_url
        end
		else
			flash.now[:danger] = "Email/Password not valid"		
  		render 'new'
  	end
  end

  def destroy
    log_out_trendz_user if logged_in?
    redirect_to root_url
  end
end
