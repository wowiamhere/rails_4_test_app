class PostsController < ApplicationController
  before_action :auth, only: [:create, :destroy, :edit]

#blog pages (mostly dynamic)
	def home
    @userPosts = Post.includes(comments: :fashion_trendz_user).paginate(page: params[:page], per_page: 6).order('created_at DESC')

# for use in partials to create a comment and save it
#   if the user is loggedi in    
    new_comment

    respond_to do |format|
      format.html
      format.js
    end

	end
  def dressesAndJeans
    @dressesAndJeans = post_lookup("dressesAndJeans")

# for use in partials to create a comment and save it
#   if the user is loggedi in
    new_comment
  end
  def handbags
    @handbags = post_lookup("handbags")

# for use in partials to create a comment and save it
#   if the user is loggedi in    
    new_comment
  end
  def makeup
    @makeup = post_lookup("makeup")

# for use in partials to create a comment and save it
#   if the user is loggedi in    
    new_comment
  end
  def shoes
    @shoes = post_lookup("shoes")

# for use in partials to create a comment and save it
#   if the user is loggedi in    
    new_comment
  end
  def show
    @viewPost = Post.includes(comments: :fashion_trendz_user).where(id: params["id"])

# for use in partials to create a comment and save it
#   if the user is loggedi in    
    new_comment    
  end

# creating/editing/destroying posts
  def create
    @post = Post.new(post_params)
    if @post.save
      flash[:success] = "Post Created!"

# need to change to to elsewhere but not know where yet      
      redirect_to root_url
    else
      flash[:danger] = "Problem with submission! Post not created!"

# need to change to to elsewhere but not know where yet      
      redirect_to root_url
    end
  end
  def edit

  end
  def destroy
  end

  private

  # to restrict parameters allowed
  def post_params
    params.require(:post).permit(:title, :description, :category, :content)
  end

  def new_comment
    unless !logged_in?
      @newCom = current_fashion_trendz_user.comments.build
    end

  end

  def post_lookup(category)
    Post.includes(comments: :fashion_trendz_user).where(category: category).paginate(page: params[:page], per_page: 4).order(created_at: :desc)
  end

end
