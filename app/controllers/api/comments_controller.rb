# API
# 	-to interact with AngularJS (frontend)
# 	-to handle viewing/editing/creating
# 		comments via front-end from
# 		various pages within application

class Api::CommentsController < ApplicationController
	# respond_to :json

	def index
		@coms = Comment.where(:created_at => (params['datUp'])..(params['datLow']), :fashion_trendz_user_id => params['usrid'])
		render json: @coms
		#respond_with @coms
	end

	def show
		@com = Comment.find(params[:id])
		render json: @com
		# respond_with @com

=begin		
		respond_to do |format|
			format.html
			format.json { render json: @com}
		end
=end
	end

	def create
		@com = Comment.new(createParams)

		if @com.save
			render json: @com, status: :created, location: @com
		else
			render json: @com.errors, status: :unprocessable_entity
		end
	end

	def update
		@com = Comment.find(params[:id])

		if @com.update_attributes(updateParams)
			head :no_content
		else
			render json: @com.errors, status: :unprocessable_entity
		end
	end

	def destroy
		@com = Comment.find(params[:id])
		@com.destroy
		head :no_content
	end

	private

		#for parametesrs to create comment
		def createParams
			params.require(:comment).permit(:content, :fashion_trendz_user_id, :post_id)
		end
		def indxParams
			params.require(:datPar).permit(:datUp, :datLow)
		end
		def updateParams
			params.permit(:id, :content, :fashion_trendz_user_id, :post_id, :updated_at, :post_id)
		end
end