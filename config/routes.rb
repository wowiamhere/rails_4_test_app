Rails.application.routes.draw do
  get 'trendz_sessions/new'

  root 'posts#home'

# get 'advice'  => 'latest_fashion_trendz_blog_static_pages#advice'
  get 'guide'   => 'latest_fashion_trendz#guide', path: 'la-la'
  get 'bios'    => 'latest_fashion_trendz#bios'
  get 'about'   => 'latest_fashion_trendz#about'
  get 'contact' => 'latest_fashion_trendz#contact'

# more dinamic pages. posts controller
  get 'home'            => 'posts#home'
  get 'dressesAndJeans' => 'posts#dressesAndJeans'
  get 'handbags'        => 'posts#handbags'
  get 'makeup'          => 'posts#makeup'
  get 'shoes'           => 'posts#shoes'

  get 'signup'  => 'fashion_trendz_users#new'

# for sessions with some resources from model
  get     'login'    => 'trendz_sessions#new'
  post    'login'    => 'trendz_sessions#create'
  delete  'logout'   => 'trendz_sessions#destroy'

# for comments
  get     'index'    => 'comments#index'

# for users resource 
  resources :fashion_trendz_users #, path: 'fashion-trendz-users'

# for account activations
  resources :account_activations, only: [:edit]

# for password resets
  resources :password_resets,     only: [:new, :create, :edit, :update]

# for posts
  resources :posts,               only: [:show, :edit, :update, :create, :destroy]

# for comments
  resources :comments

# rails testing things out 
 namespace :api do 
  resources :comments #, defaults: { format: :json }
end

 match '*path' => 'posts#show', via: [:get, :post]

# rails testing things out

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
