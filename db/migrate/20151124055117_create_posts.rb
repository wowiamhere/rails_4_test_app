class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :content
      t.string :category

      t.timestamps null: false
    end

    add_index :posts, [:category, :created_at]
  end
end
