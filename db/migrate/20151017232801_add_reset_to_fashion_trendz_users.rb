class AddResetToFashionTrendzUsers < ActiveRecord::Migration
  def change
    add_column :fashion_trendz_users, :reset_digest, :string
    add_column :fashion_trendz_users, :reset_sent_at, :datetime
  end
end
