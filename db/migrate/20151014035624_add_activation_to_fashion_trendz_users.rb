class AddActivationToFashionTrendzUsers < ActiveRecord::Migration
  def change
    add_column :fashion_trendz_users, :activation_digest, :string
    add_column :fashion_trendz_users, :activated, :boolean, default: false
    add_column :fashion_trendz_users, :activated_at, :datetime
  end
end
