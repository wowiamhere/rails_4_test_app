class AddAdminToFashionTrendzUsers < ActiveRecord::Migration
  def change
    add_column :fashion_trendz_users, :admin, :boolean, default: false
  end
end
