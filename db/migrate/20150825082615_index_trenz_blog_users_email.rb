class IndexTrenzBlogUsersEmail < ActiveRecord::Migration
  def change
  	add_index :fashion_trendz_users, :email, unique: true
  end
end
