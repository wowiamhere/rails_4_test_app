=begin
=end
FashionTrendzUser.create!(name: "example name", email: "example@trendzblog.com", password: 'password', password_confirmation: 'password', admin: true, activated: true, activated_at: Time.zone.now )

99.times do |n|
	nm = Faker::Name.name
	em = "examples-#{n+1}@trendzblog.com"
	ps = 'password'
	FashionTrendzUser.create!(name: nm, email: em, password: ps, password_confirmation: ps, activated: true, activated_at: Time.zone.now )
end


# seeding posts
# 	20 posts per category (4) = total 80 posts of
#   various lenghts
20.times do

	# resetting n
	n = 3
	5.times do
		post = Faker::Lorem.paragraph(n)
		Post.create!(content: post, category: "makeup", title: "MakeUp Ideas!", description: "A post about MakeUp application.")

		# to vary length/content of comments
		n += 3
	end

# resetting n
	n = 12
	5.times do
		post = Faker::Lorem.paragraph(n)
		Post.create!(content: post, category: "shoes", title: "Shoes and Stuff", description: "How to put on shoes")

		# to vary length/content of comments
		n -= 2
	end

# resetting n
	n = 4
	5.times do
		post = Faker::Lorem.paragraph(n)
		Post.create!(content: post, category: "handbags", title: "fendi's gem", description: "Oooops! You can't afford it ... so can't I")

		# to vary length/content of comments
		n += 3
	end	

# resetting n
	n = 3
	5.times do
		post = Faker::Lorem.paragraph(n)
		Post.create!(content: post, category: "dressesAndJeans", title: "Best Jeans for This Season", description: "Exellent fahsion choices for Jeans")

		# to vary length/content of comments
		n += 3
	end

=begin
			CONTINUE LATER
	choice = {
		"cat" : ["dressesAndJeans", "handbags", "makeup", "shoes"],
		"title" : ["Best Season Jeans", "Fendi's Gem", "Makeup tips", "Shoes And Stuff"]
		"desc" : ["xellent fahsion choices for Jeans", "Oooops! You can't afford it ... so can't I", "A post about MakeUp application.", "How to put on shoes"]
	}

	choice.title.each do 
		post = Faker::Lorem.paragraph(n)
		Post.create!(content: post, category: choice.title.cat, title: "Best Jeans for This Season", description: "Exellent fahsion choices for Jeans")		
	end
=end

end

# seeding comments for posts
# 	7 users comment on 10 posts


# for ammount of sentences
n = 1
users = FashionTrendzUser.take(7)

# categories of posts
cats = ["dressesAndJeans", "handbags", "makeup", "shoes"]

cats.each do |cat|
	psts = Post.where(category: cat).take(10)

	psts.each do |pst|
		users.each do |user|
			# comments with n sentences
			comm = Faker::Lorem.sentences(n)

			user.comments.create!(content: comm, fashion_trendz_user_id: user.id, post_id: pst.id)
			
			# to vary length/content of comments
			n += 2

			com = Faker::Lorem.sentences(n)

		end

		# resetting number of comment sentences (n)
		n = 1

	end
end
